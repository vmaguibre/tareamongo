﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Entities;
using MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Configuration;

namespace WebApplication1.Models
{
    public class AccountModel
    {
        private MongoClient mongoClient;
        private IMongoCollection<Account> accountCollection;
        public AccountModel()
        {
            mongoClient = new MongoClient(ConfigurationManager.AppSettings["mongoDBHost"]);
            var db = mongoClient.GetDatabase(ConfigurationManager.AppSettings["mongoDBName"]);
            accountCollection = db.GetCollection<Account>("pelicula");
        
        }
        public List<Account> findAll()
        {
            return accountCollection.AsQueryable<Account>().ToList();
        }
        public Account find(string id)
        {
            var peliculaId = new ObjectId(id);
            return accountCollection.AsQueryable<Account>().SingleOrDefault(a => a.Id == peliculaId);
        }
        public void create(Account pelicula)
        {
            accountCollection.InsertOne(pelicula);
        }
        public void update(Account pelicula)
        {
            accountCollection.UpdateOne(
                Builders<Account>.Filter.Eq("_id",pelicula.Id),
                Builders<Account>.Update
                .Set("Nombre",pelicula.Nombre)
                .Set("Género", pelicula.Género)
                .Set("Director", pelicula.Director)
                .Set("Franquicia", pelicula.Franquicia)
                .Set("País", pelicula.País)
                .Set("Año", pelicula.Año)
                .Set("Duración", pelicula.Duración)
                .Set("Compañia_Productora", pelicula.Compañia_Productora)
                );
        }
        public void delete(string id)
        {
            accountCollection.DeleteOne(Builders<Account>.Filter.Eq("_id",ObjectId.Parse(id)));
        }
        public List<Account> find_name(string nombre)
        {
            return accountCollection.AsQueryable<Account>.find(a => a.Nombre == nombre);
        }
    }   
}
