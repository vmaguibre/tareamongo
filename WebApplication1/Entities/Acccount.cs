﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities

{
    public class Account
    {
        [BsonId]
        public ObjectId Id
        {
            get;
            set;
        }
        [BsonElement("Nombre")]
        public string Nombre
        {
            get;
            set;
        }
        [BsonElement("Género")]
        public string Género
        {
            get;
            set;
        }

        [BsonElement("Director")]
        public string Director
        {
            get;
            set;
        }
        [BsonElement("Franquicia")]
        public string Franquicia
        {
            get;
            set;
        }
        [BsonElement("País")]
        public string País
        {
            get;
            set;
        }
        [BsonElement("Año")]
        public int Año
        {
            get;
            set;
        }
        [BsonElement("Duración")]
        public int Duración
        {
            get;
            set;
        }
        [BsonElement("Compañia_Productora")]
        public string Compañia_Productora
        {
            get;
            set;
        }


        [BsonElement("Actores")]
        public IList<string> Actores
        {
            get;
            set;
        }
    }
}