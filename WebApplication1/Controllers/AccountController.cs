﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Entities;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        private AccountModel accountModel = new AccountModel();
        public ActionResult Index()
        {
            ViewBag.accounts = accountModel.findAll();
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View("Add", new Account());
        }
        [HttpPost]
        public ActionResult Add(Account account)
        {
            accountModel.create(account);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(string id)
        {
            accountModel.delete(id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(string id)
        {
            return View("Edit",accountModel.find(id));
        }
        [HttpPost]
        public ActionResult Edit(Account pelicula, FormCollection fc)
        {
            string id = fc["Id"];
            var currentAccount = accountModel.find(id);
                currentAccount.Nombre = pelicula.Nombre;
                currentAccount.Género = pelicula.Género;
                currentAccount.Director = pelicula.Director;
                currentAccount.Franquicia = pelicula.Franquicia;
                currentAccount.País = pelicula.País;
                currentAccount.Año = pelicula.Año;
                currentAccount.Duración = pelicula.Duración;
                currentAccount.Compañia_Productora = pelicula.Compañia_Productora;
                accountModel.update(currentAccount);
                return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Find()
        {
            return View("Find", new Account());
        }
        [HttpGet]
        public ActionResult FindForName()
        {
            return View("FindForName", new Account());
        }
        [HttpPost]
        public ActionResult FindForNameResult(string name)
        {
            ViewBag.resultado_por_nombre = accountModel.find_name(name);
            return View();
        }
    }
}